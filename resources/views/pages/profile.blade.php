@extends('layouts.app')

@section('content')
    <h2>
        sascha mikula
    </h2>

    <p>
        dipl.-ing. (fh) innenarchitektur
    </p>
    <p>
        *1971 in nürnberg
    </p>

    <ul>
        <li>
            studium th-nürnberg 1996-1998
        </li>

        <li>
            innenausbau, lichtdesign 1998-2001
        </li>

        <li>
            studium fh-rosenheim 2001-2006
        </li>

        <li>
            greenmeadow architects 2003-2004
        </li>

        <li>
            bsk designhaus, nürnberg 2007-2009
        </li>

        <li>
            sm-innenarchitektur 2009-heute
        </li>
    </ul>

    <br>
    
    <h2>leistungen</h2>

    <p>
        beratung, entwurf, planung, konzeptentwicklung, betreuung
    </p>

    <p>
        bauaufnahme, umbau, möblierung, projektsteuerung, lichtplanung, bauüberwachung, modellbau, mediengestaltung,
        dokumentation
    </p>

    <p>
        messe, praxen, öffentliche und private bauten, büros, ladenbau
    </p>
@endsection
