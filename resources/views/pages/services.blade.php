@extends('layouts.app')

@section('content')
    <p>
        beratung, entwurf, planung, konzeptentwicklung, betreuung
    </p>

    <p>
        bauaufnahme, umbau, möblierung, projektsteuerung, lichtplanung, bauüberwachung, modellbau, mediengestaltung,
        dokumentation
    </p>

    <p>
        messe, praxen, öffentliche und private bauten, büros, ladenbau
    </p>
@endsection
