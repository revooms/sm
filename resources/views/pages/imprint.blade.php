@extends('layouts.app')

@section('content')

    @include('components.address')

    <p>
        alle rechte vorbehalten.
        <br>
        die urheberrechte dieser website liegen vollständig bei sm-innenarchitektur.
    </p>

    <p>
        fotos, texte, pläne - sascha mikula
        <br>
        visualisierungen - studioA
    </p>

@endsection
