<div class="col-12 col-sm-6">
    <div id="carouselExampleFade" class="carousel carousel-dark slide carousel-fade" data-bs-ride="carousel">
        <div class="carousel-indicators">
            @foreach ($items as $slide)
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="{{ $loop->index }}"
                        @if ($loop->first) class="active" aria-current="true" @endif
                        aria-label="Slide {{ $loop->index + 1 }}"></button>
            @endforeach
        </div>
        <div class="carousel-inner">
            @foreach ($items as $slide)
                <div class="carousel-item @if ($loop->first) active @endif">
                    <img src="{{ $slide }}" class="d-block w-100" alt="...">
                </div>
            @endforeach
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">zurück</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">vor</span>
        </button>
    </div>
</div>
