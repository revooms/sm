<nav class="navbar navbar-expand-md navbar-light bg-white">
    <div class="container-fluid">
        <button class="navbar-toggler collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#navbar-content">
            <div class="hamburger-toggle">
                <div class="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </button>
        <div class="collapse navbar-collapse" id="navbar-content">
            <ul class="navbar-nav mr-auto mb-2 mb-lg-0">
                <li class="nav-item mr-sm-4">
                    <a class="nav-link" href="{{ route('pages.home') }}">
                        büro
                    </a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown"
                       data-bs-auto-close="outside">projekte</a>
                    <ul class="dropdown-menu shadow">
                        <li><a class="dropdown-item" href="{{ route('projects.binder') }}">binder theke</a></li>
                        <li><a class="dropdown-item" href="{{ route('projects.citypark') }}">city park center</a></li>
                        <li><a class="dropdown-item" href="{{ route('projects.darstellen') }}">darstellen</a></li>
                        <li><a class="dropdown-item" href="{{ route('projects.dkdl') }}">dkdl</a></li>
                        <li><a class="dropdown-item" href="{{ route('projects.eichler') }}">eichler homes</a></li>
                        <li><a class="dropdown-item" href="{{ route('projects.gillitzer') }}">gillitzer</a></li>
                        <li class="dropend">
                            <a href="#" class="dropdown-item dropdown-toggle" data-bs-toggle="dropdown"
                               data-bs-auto-close="outside">modellbau</a>
                            <ul class="submenu dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('projects.model.badimberg') }}">bad im
                                        berg</a></li>
                                <li><a class="dropdown-item" href="{{ route('projects.model.eichler') }}">eichler
                                        homes</a></li>
                                <li><a class="dropdown-item"
                                       href="{{ route('projects.model.fallingwater') }}">falling water</a>
                                <li><a class="dropdown-item" href="{{ route('projects.model.lecorbusier') }}">le
                                        corbusier</a></li>
                            </ul>
                        </li>
                        <li><a class="dropdown-item" href="{{ route('projects.sealsystems') }}">seal systems</a></li>
                        <li class="dropend">
                            <a href="#" class="dropdown-item dropdown-toggle" data-bs-toggle="dropdown"
                               data-bs-auto-close="outside">sebalder höfe</a>
                            <ul class="submenu dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('projects.sebald.breidung') }}">praxis
                                        dr. breidung</a>
                                <li><a class="dropdown-item" href="{{ route('projects.sebald.eisgruber') }}">praxis
                                        dr. eisgruber, dr. ertel, dr. heim</a>
                            </ul>
                        </li>
                        <li><a class="dropdown-item" href="{{ route('projects.stadtregal') }}">stadtregal</a></li>
                        <li><a class="dropdown-item" href="{{ route('projects.zbau') }}">z-bau</a></li>
                        <li><a class="dropdown-item" href="{{ route('projects.zitzmann') }}">zitzmann</a></li>
                    </ul>
                </li>
                <li class="nav-item ml-4">
                    <a class="nav-link" href="{{ route('pages.imprint') }}">impressum</a>
                </li>

            </ul>
        </div>
    </div>
</nav>
@push('scripts')
    <script>
        document.addEventListener('click', function(e) {
            // Hamburger menu
            if (e.target.classList.contains('hamburger-toggle')) {
                e.target.children[0].classList.toggle('active');
            }
        })
    </script>
@endpush
