<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ $title }} | Sascha Mikula</title>

    <link rel="stylesheet" href="/css/app.css">

    <meta name="keywords"
          content="architektur, innenarchitektur, innenausbau, innenraum, innenraumgestaltung, raumgestaltung, raumkonzepte, bau, entwurf, planung, konzeptentwicklung, konzepte, bauaufnahme, umbau, möblierung, moeblierung, projektsteuerung, lichtplanung, lichtkonzepte, bauueberwachung, bauleitung, modellbau, medien, webdesingn, webhosting, grafikdesingn, gestaltung, dokumentationen, gastronomie, ladenbau, messe, standplanung, praxen, arztpraxis, büros, buero, kanzleien,wellness, bsk, stadtregal, studioa, studio a, studio-a, city park center, dkdl, die krieger des lichts, sealsystems, seal systems, seal-systems,sebalder hoefe, einkaufsscenter, zbau, shoppen in nürnberg, nuernberg, doktor breidung, doktor heim, doktor ertel, doktor eisgruber, sm-innenarchitektur, susann velte, sascha mikula, z-bau, zitzmann, wohnen und leben, Facharztzentrum, Sebaldus-Viertel, Rathenauplatz, Wohn- und Ladeneinheiten, buero- und projekteinrichtung, wohndesign, kreative konzepte, wohnloesungen, buerolösungen, ladengestaltung, nürnberg" />
    <meta name="description" content="Innenarchitektur Nürnberg">

</head>

<body class="antialiased">

    <div class="d-flex vh-100">

        <aside class="col-2 d-none d-sm-flex bg-blue flex-column align-items-start" style="width:220px;">

            <div class="header mb-auto align-self-end">
                <img src="/img/kopf-mitstrich.png" alt="Kopf Sascha Mikula">
            </div>

            <div class="address p-2 text-white align-self-end">
                @include('components.address')
            </div>

        </aside>

        <main class="col-sm-10 bg-white">
            @include('components.navigation')
            <div class="p-3" style="height: auto">
                <h1>{{ $title }}</h1>
                @yield('content')
            </div>
        </main>

    </div>


    <script src="/js/app.js"></script>

    @stack('scripts')
</body>

</html>
