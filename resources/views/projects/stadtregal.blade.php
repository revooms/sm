@extends('layouts.app')

@section('content')
    gestaltung, planung und bauüberwachung des verkaufs- und showrooms.

    @include('components.slide', [
        'items' => [
            '/img/stadtregal/sr01.jpg',
            '/img/stadtregal/sr03.jpg',
            '/img/stadtregal/sr04.jpg',
            '/img/stadtregal/sr05.jpg',
            '/img/stadtregal/sr06.jpg',
            '/img/stadtregal/sr07.jpg',
            '/img/stadtregal/sr08.jpg',
            '/img/stadtregal/sr09.jpg',
        ],
    ])
@endsection
