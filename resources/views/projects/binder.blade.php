@extends('layouts.app')

@section('content')
    umsetzung und bauleitung der vier stockwerke.
    <br>
    pläne und visualisierungen von studioA.

    @include('components.slide', [
        'items' => [
            '/img/bindertheke/bg01.jpg',
            '/img/bindertheke/bg02.jpg',
            '/img/bindertheke/bg04.jpg',
            '/img/bindertheke/bg05.jpg',
            '/img/bindertheke/bg06.jpg',
            '/img/bindertheke/bg10.jpg',
            '/img/bindertheke/bg11.jpg',
            '/img/bindertheke/bg12.jpg',
            '/img/bindertheke/bg20.jpg',
            '/img/bindertheke/bg21.jpg',
        ],
    ])
@endsection
