@extends('layouts.app')

@section('content')
    entwurfsskizzen für küchen-, wohnstudios und handwerksbetriebe.
    <br>
    entwurf und visualisierung einer cafeteria.
    <br>
    gestaltung des booklets.
    @include('components.slide', [
        'items' => [
            '/img/darstellen/adbk10.jpg',
            '/img/darstellen/adbk11.jpg',
            '/img/darstellen/adbk12.jpg',
            '/img/darstellen/adbk13.jpg',
            '/img/darstellen/badimberg.jpg',
            '/img/darstellen/darstellen1.jpg',
            '/img/darstellen/darstellen2.jpg',
        ],
    ])
@endsection
