@extends('layouts.app')

@section('content')
    konzept und entwurfsskizzen der gillitzer einkaufspassage.
    @include('components.slide', [
        'items' => [
            '/img/gillitzerpassage/gillitzer1.jpg',
            '/img/gillitzerpassage/gillitzer2.jpg',
            '/img/gillitzerpassage/gillitzer3.jpg',
            '/img/gillitzerpassage/gillitzer4.jpg',
            '/img/gillitzerpassage/gillitzer5.jpg',
            '/img/gillitzerpassage/gillitzer6.jpg',
            '/img/gillitzerpassage/gillitzer7.jpg',
        ],
    ])
@endsection
