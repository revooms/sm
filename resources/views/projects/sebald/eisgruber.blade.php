@extends('layouts.app')

@section('content')
    dr. heim, ertel, eisgruber
    <br>
    planung und möblierung in zusammenarbeit mit h. gräßer.

    @include('components.slide', [
        'items' => [
            '/img/sebalderhoefe/drheim-ertel-eisgruber/hee01.jpg',
            '/img/sebalderhoefe/drheim-ertel-eisgruber/hee02.jpg',
            '/img/sebalderhoefe/drheim-ertel-eisgruber/hee03.jpg',
            '/img/sebalderhoefe/drheim-ertel-eisgruber/hee04.jpg',
        ],
    ])
@endsection
