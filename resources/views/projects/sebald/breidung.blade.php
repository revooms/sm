@extends('layouts.app')

@section('content')

    dr. breidung
    <br>
    planung und möblierung in zusammenarbeit mit h. gräßer.
    
    @include('components.slide', [
        'items' => [
            '/img/sebalderhoefe/drbreidung/br01.jpg',
            '/img/sebalderhoefe/drbreidung/br02.jpg',
            '/img/sebalderhoefe/drbreidung/br03.jpg',
            '/img/sebalderhoefe/drbreidung/br04.jpg',
            '/img/sebalderhoefe/drbreidung/br05.jpg',
        ],
    ])
@endsection
