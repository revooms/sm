@extends('layouts.app')

@section('content')
    entwurf und neugestaltung des eingangsbereiches und der büroräume.

    @include('components.slide', [
        'items' => [
            '/img/sealsystems/ss01.jpg',
            '/img/sealsystems/ss02.jpg',
            '/img/sealsystems/ss03.jpg',
            '/img/sealsystems/ss04.jpg',
            '/img/sealsystems/ss05.jpg',
            '/img/sealsystems/ss06.jpg',
            '/img/sealsystems/ss07.jpg',
            '/img/sealsystems/ss08.jpg',
            '/img/sealsystems/ss09.jpg',
        ],
    ])
@endsection
