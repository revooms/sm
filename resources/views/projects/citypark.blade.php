@extends('layouts.app')

@section('content')
    entwurf und darstellung der raumstruktur in zusammenarbeit mit h. gräßer.
    @include('components.slide', [
        'items' => [
            '/img/cityparkcenter/cityparkcenter-1.jpg',
            '/img/cityparkcenter/cityparkcenter-2.jpg',
            '/img/cityparkcenter/cityparkcenter-3.jpg',
            '/img/cityparkcenter/cityparkcenter-5.jpg',
            '/img/cityparkcenter/cityparkcenter-6.jpg',
        ],
    ])
@endsection
