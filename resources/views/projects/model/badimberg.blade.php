@extends('layouts.app')

@section('content')
    modelle zu "bad im berg".

    @include('components.slide', [
        'items' => [
            '/img/modellbau/badimberg/bib01.jpg',
            '/img/modellbau/badimberg/bib10.jpg',
            '/img/modellbau/badimberg/bib11.jpg',
            '/img/modellbau/badimberg/bib12.jpg',
        ],
    ])
@endsection
