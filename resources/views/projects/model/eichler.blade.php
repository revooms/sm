@extends('layouts.app')

@section('content')
    verschiedene eichler home modelle.
    @include('components.slide', [
        'items' => [
            '/img/modellbau/eichler/eich01.jpg',
            '/img/modellbau/eichler/eich02.jpg',
            '/img/modellbau/eichler/eich03.jpg',
        ],
    ])
@endsection
