@extends('layouts.app')

@section('content')
    modellbau und präsentation, villa savoye, le corbusier.

    @include('components.slide', [
        'items' => [
            '/img/modellbau/lecorbusier/lc01.jpg',
            '/img/modellbau/lecorbusier/lc02.jpg',
            '/img/modellbau/lecorbusier/lc03.jpg',
            '/img/modellbau/lecorbusier/lc04.jpg',
        ],
    ])
@endsection
