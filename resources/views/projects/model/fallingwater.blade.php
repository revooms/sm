@extends('layouts.app')

@section('content')
    modellbau und präsentation, falling water, frank lloyd wright.

    @include('components.slide', [
        'items' => ['/img/modellbau/fallingwater/fw01.jpg', '/img/modellbau/fallingwater/fw02.jpg'],
    ])
@endsection
