@extends('layouts.app')

@section('content')
    alle leistungsphasen, greenmeadow architects california.
    @include('components.slide', [
        'items' => [
            '/img/eichlerhomes/eichler01.jpg',
            '/img/eichlerhomes/eichler02.jpg',
            '/img/eichlerhomes/eichler03.jpg',
            '/img/eichlerhomes/eichler04.jpg',
            '/img/eichlerhomes/eichler05.jpg',
            '/img/eichlerhomes/eichler06.jpg',
            '/img/eichlerhomes/eichler07.jpg',
            '/img/eichlerhomes/eichler08.jpg',
            '/img/eichlerhomes/eichler09.jpg',
            '/img/eichlerhomes/eichler10.jpg',
        ],
    ])
@endsection
