@extends('layouts.app')

@section('content')
    entwurf, planung, visualisierung und modellbau eines gesamtheitlichen konzepts für die z-bau räumlichkeiten.

    @include('components.slide', [
        'items' => [
            '/img/z-bau/z.bau12.jpg',
            '/img/z-bau/z-bau01.jpg',
            '/img/z-bau/z-bau02.jpg',
            '/img/z-bau/z-bau03.jpg',
            '/img/z-bau/z-bau04.jpg',
            '/img/z-bau/z-bau05.jpg',
            '/img/z-bau/z-bau06.jpg',
            '/img/z-bau/z-bau07.jpg',
            '/img/z-bau/z-bau08.jpg',
            '/img/z-bau/z-bau09.jpg',
            '/img/z-bau/z-bau10.jpg',
            '/img/z-bau/z-bau11.jpg',
        ],
    ])
@endsection
