@extends('layouts.app')

@section('content')
    grundriss, entwurf für die räumlichkeiten der krieger des lichts.
    @include('components.slide', [
        'items' => ['/img/dkdl/dkdl-1.jpg', '/img/dkdl/dkdl-2.jpg'],
    ])
@endsection
