<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>sm-architektur, Sascha Mikula</title>

    <link rel="stylesheet" href="/css/app.css">

    <meta name="keywords"
          content="architektur, innenarchitektur, innenausbau, innenraum, innenraumgestaltung, raumgestaltung, raumkonzepte, bau, entwurf, planung, konzeptentwicklung, konzepte, bauaufnahme, umbau, möblierung, moeblierung, projektsteuerung, lichtplanung, lichtkonzepte, bauueberwachung, bauleitung, modellbau, medien, webdesingn, webhosting, grafikdesingn, gestaltung, dokumentationen, gastronomie, ladenbau, messe, standplanung, praxen, arztpraxis, büros, buero, kanzleien,wellness, bsk, stadtregal, studioa, studio a, studio-a, city park center, dkdl, die krieger des lichts, sealsystems, seal systems, seal-systems,sebalder hoefe, einkaufsscenter, zbau, shoppen in nürnberg, nuernberg, doktor breidung, doktor heim, doktor ertel, doktor eisgruber, sm-innenarchitektur, susann velte, sascha mikula, z-bau, zitzmann, wohnen und leben, Facharztzentrum, Sebaldus-Viertel, Rathenauplatz, Wohn- und Ladeneinheiten, buero- und projekteinrichtung, wohndesign, kreative konzepte, wohnloesungen, buerolösungen, ladengestaltung, nürnberg" />
    <meta name="description" content="Innenarchitektur Nürnberg">

</head>

<body class="antialiased">

    <div class="d-flex vw-100 vh-100 justify-content-center align-items-center">
        <div>
            <a href="{{ route('pages.philosophy') }}"><img src="/img/startbild.png" alt="Kopf Sascha Mikula"
                     style="width:472px"></a>
        </div>
    </div>

</body>

</html>
