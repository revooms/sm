<?php

use Illuminate\Support\Facades\Route;

Route::view('/', 'pages.profile', ['title' => 'sm-innenarchitektur'])->name('pages.home');
Route::view('/impressum', 'pages.imprint', ['title' => 'impressum'])->name('pages.imprint');
Route::view('/leistungen', 'pages.services', ['title' => 'leistungen'])->name('pages.services');
Route::view('/philosophie', 'pages.philosophy', ['title' => 'philosophie'])->name('pages.philosophy');
Route::view('/profil', 'pages.profile', ['title' => 'profil'])->name('pages.profile');

Route::prefix('projekte')->group(function () {
    Route::view('binder-theke', 'projects.binder', ['title' => 'binder theke, wohnen und arbeiten'])->name('projects.binder');
    Route::view('city-park-center', 'projects.citypark', ['title' => 'city park center'])->name('projects.citypark');
    Route::view('darstellen', 'projects.darstellen', ['title' => 'darstellen'])->name('projects.darstellen');
    Route::view('dkdl', 'projects.dkdl', ['title' => 'dkdl, werbeagentur'])->name('projects.dkdl');
    Route::view('eichler-homes', 'projects.eichler', ['title' => 'eichler homes'])->name('projects.eichler');
    Route::view('gillitzer-passage', 'projects.gilitzer', ['title' => 'gillitzer passage'])->name('projects.gillitzer');
    Route::prefix('modellbau')->group(function () {
        Route::view('bad-im-berg', 'projects.model.badimberg', ['title' => 'bad im berg'])->name('projects.model.badimberg');
        Route::view('eichler-homes', 'projects.model.eichler', ['title' => 'eichler homes'])->name('projects.model.eichler');
        Route::view('falling-water', 'projects.model.fallingwater', ['title' => 'falling water'])->name('projects.model.fallingwater');
        Route::view('le-corbusier', 'projects.model.lecorbusier', ['title' => 'le corbusier'])->name('projects.model.lecorbusier');
    });
    Route::view('seal-systems', 'projects.sealsystems', ['title' => 'seal systems'])->name('projects.sealsystems');
    Route::prefix('sebalder-hoefe')->group(function() {
        Route::view('praxis-dr-breidung', 'projects.sebald.breidung', ['title' => 'sebalder höfe, arztpraxen'])->name('projects.sebald.breidung');
        Route::view('praxis-dr-eisgruber', 'projects.sebald.eisgruber', ['title' => 'sebalder höfe, arztpraxen'])->name('projects.sebald.eisgruber');

    });
    Route::view('stadtregal', 'projects.stadtregal', ['title' => 'stadtregal, bsk filiale ulm'])->name('projects.stadtregal');
    Route::view('z-bau', 'projects.zbau', ['title' => 'z-bau, kunst und event'])->name('projects.zbau');
    Route::view('zitzmann', 'projects.zitzmann', ['title' => 'zitzmann büromöbel'])->name('projects.zitzmann');
});
